from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from elektrodefter.sitemaps import MySiteMap
from . import views
from django.conf.urls.static import static
from django.conf import settings
import readermode

sitemaps = {'Entries' : MySiteMap()}

urlpatterns = [
    # Examples:
    # url(r'^$', 'elektrodefter.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', admin.site.urls),
    url(r'^$', views.index, name='index'),
    url(r'^main/', include('readermode.urls')),
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps},
    name='django.contrib.sitemaps.views.sitemap'),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^results/', readermode.views.showSearchResults, name="showsearchresults")

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

