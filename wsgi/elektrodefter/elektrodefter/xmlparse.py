# -*-coding:utf-8 -*-

from xml.dom import minidom
import codecs
from tidylib import tidy_document
from entrysystem.models import Entry
from datetime import datetime
from readermode.models import Category
import django
django.setup()
XMLfile = codecs.open('elektrodefter.wordpress.2015-02-02.xml', encoding='utf-8')
fullcontent = XMLfile.read().encode('utf-8')
dom = minidom.parseString(fullcontent)
entrylist = Entry.objects.all()
entrylistTitles = []

for entry in entrylist:
    entrylistTitles.append(entry.title)

def getText(nodelist):
    rc = []
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.data)
    return ''.join(rc)

def handleItems(string):
    items = string.getElementsByTagName("item")
    
    for item in items:
    
        if getText(item.getElementsByTagName("wp:post_type")[0].childNodes) == "post":
            title = getText(item.getElementsByTagName("title")[0].childNodes).encode("utf-8")
            
            if title not in entrylistTitles:
                global entry
                entry = Entry()
                entry.title = title
                normalizedtitle = (title.lower()).replace(' ', '_').replace(':', '').replace('ı', 'i').replace('ü', 'u').replace('İ', 'i')
                normalizedtitle = normalizedtitle.replace('ç', 'c').replace('ö', 'o').replace('ğ', 'g').replace('ş', 's').replace('Ş', 's')
                normalizedtitle = normalizedtitle.replace('.', '').replace('!', '').replace('?', '').replace(',', '').replace('Ö', 'o').replace('Ç', 'c')
                filepath = "articles/" + normalizedtitle + ".html"
                entry.entry_path = filepath
                entry.url_name = normalizedtitle
                
                with open(filepath, 'w') as fil:
                    fil.write("""{% extends 'entrysystem/entrywrapper.html' %}\n{% load staticfiles %}\n{% block entryblock %}\n""")

                    handleCDATA(item, fil)
                    handleDates(item, fil)
                    handleCategory(item, fil)
                    entry.save()
                    fil.write("\n{% endblock %}")


    

def handleCDATA(item, fil):
    cdata = (item.getElementsByTagName("content:encoded")[0].toxml()[26:-21]).replace("wp-content/uploads", "static/images").encode("utf-8")
    options = dict(char_encoding='utf8')
    cdata = tidy_document(cdata, options=options)[0]
    entry.content = cdata
    fil.write(cdata)

def handleDates(item, fil):
    post_date = getText(item.getElementsByTagName("wp:post_date")[0].childNodes).encode("utf-8")
    fil.write("\nYazılma Tarihi: " + post_date)
    post_date_split = post_date.split("-")
    clocktime = post_date_split[2][post_date_split[2].find(" ")+1:].split(":")
    post_date_split[2] = post_date_split[2][:post_date_split[2].find(" ")]
    post_date_split.extend(clocktime)
    converted_dates = []
    for i in post_date_split:
        converted_dates.append(int(i))

    entry.time_created = datetime(converted_dates[0], converted_dates[1], converted_dates[2] 
                                  ,converted_dates[3], converted_dates[4], converted_dates[5])

        
def handleCategory(item, fil):
    category = (item.getElementsByTagName("category")[0].toxml()).encode("utf-8")
    cat_pos_start = category.find("nicename=")
    cat_pos_end = category.find('">')
    category = category[cat_pos_start+10:cat_pos_end]
    category = category.replace("basys2", "fpga")
    category = category.replace("JBox2D", "java")
    category = category.replace("c-programlama-dili", "c")
    category = category.replace("uncategorized", "diger")

    entry.category = Category.objects.get(name=category)



handleItems(dom)
