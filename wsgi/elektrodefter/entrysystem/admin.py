from django.db import models
from django import forms
from django.contrib import admin
from .models import Entry
from ckeditor_uploader.widgets import CKEditorUploadingWidget
# Register your models here.

class EntryAdmin(admin.ModelAdmin):
    formfield_overrides = { models.TextField: {'widget': CKEditorUploadingWidget}, }

    class Media:
        css = {
            "all": ("css/fixck.css",)
        }
        js = ('ckeditor/ckeditor.js',)

admin.site.register(Entry, EntryAdmin)