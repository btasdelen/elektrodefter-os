# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('readermode', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=160, verbose_name=b'entry')),
                ('time_created', models.DateTimeField(default=datetime.datetime(2015, 2, 14, 22, 9, 24, 363023), verbose_name=b'createdtime')),
                ('last_edited', models.DateTimeField(default=datetime.datetime(2015, 2, 14, 22, 9, 24, 363056), verbose_name=b'editedtime')),
                ('written_by', models.CharField(default=b'Bilal Ta\xc5\x9fdelen', max_length=50, verbose_name=b'writtenby')),
                ('summary', models.CharField(max_length=600, verbose_name=b'summary')),
                ('category', models.ForeignKey(verbose_name=b'category', to='readermode.Category')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name=b'tag')),
                ('entries', models.ManyToManyField(to='entrysystem.Entry')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
