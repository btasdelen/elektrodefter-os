# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('entrysystem', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='entry',
            name='name',
        ),
        migrations.AddField(
            model_name='entry',
            name='entry_path',
            field=models.CharField(default='/', max_length=50),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='entry',
            name='title',
            field=models.CharField(default='A', max_length=160, verbose_name=b'title'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='entry',
            name='url_name',
            field=models.CharField(default='/a', max_length=160, verbose_name=b'urlname'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='entry',
            name='last_edited',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 18, 6, 52, 21, 736441), verbose_name=b'editedtime'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='entry',
            name='time_created',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 18, 6, 52, 21, 736389), verbose_name=b'createdtime'),
            preserve_default=True,
        ),
    ]
