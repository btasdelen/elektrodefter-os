# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('entrysystem', '0002_auto_20150218_0653'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entry',
            name='last_edited',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 19, 20, 38, 46, 463202), verbose_name=b'editedtime'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='entry',
            name='summary',
            field=models.CharField(max_length=1600, verbose_name=b'summary', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='entry',
            name='time_created',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 19, 20, 38, 46, 463158), verbose_name=b'createdtime'),
            preserve_default=True,
        ),
    ]
