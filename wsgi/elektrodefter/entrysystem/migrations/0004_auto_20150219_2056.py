# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('entrysystem', '0003_auto_20150219_2038'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entry',
            name='last_edited',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 19, 20, 56, 53, 245530), verbose_name=b'editedtime'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='entry',
            name='time_created',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 19, 20, 56, 53, 245485), verbose_name=b'createdtime'),
            preserve_default=True,
        ),
    ]
