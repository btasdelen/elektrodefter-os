# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('entrysystem', '0005_auto_20150222_2023'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entry',
            name='entry_path',
            field=models.CharField(max_length=250),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='entry',
            name='last_edited',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 22, 20, 57, 54, 39876), verbose_name=b'editedtime'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='entry',
            name='time_created',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 22, 20, 57, 54, 39843), verbose_name=b'createdtime'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='entry',
            name='title',
            field=models.CharField(max_length=260, verbose_name=b'title'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='entry',
            name='url_name',
            field=models.CharField(max_length=260, verbose_name=b'urlname'),
            preserve_default=True,
        ),
    ]
