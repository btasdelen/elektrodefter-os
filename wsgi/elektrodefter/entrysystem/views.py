from django.shortcuts import render
from readermode.models import Category
from entrysystem.models import Entry
import operator
from readermode.forms import SearchForm
from django.template.context import RequestContext
from django.http.request import HttpRequest

# Create your views here.

def showentries(request, category_name):
    form = SearchForm()
    entries = sorted(Entry.objects.filter(category = Category.objects.filter(name = category_name), draft=False), key=operator.attrgetter('time_created'))
    return render(request, 'entrysystem/entrylist.html', {'entries':entries, 'form':form})

def showEntry(request, url_name):
    form = SearchForm()
    entry = Entry.objects.get(url_name=url_name)
    return render(request, 'entrysystem/entrywrapper.html', {'entry':entry, 'form':form})

def showSearchResults(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = SearchForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            search_query = form.cleaned_data['search_query']
            results = Entry.objects.filter(title__icontains=search_query, draft=False) | \
                Entry.objects.filter(content__icontains=search_query, draft=False)
            return render(request,"results.html", {'results' : results, 'form' : form}, context_instance=RequestContext(request))
    
    else:
        form = SearchForm()

    results = Entry.objects.filter(draft=False)
    return render(request,"results.html", {'results' : results, 'form' : form}, context_instance=RequestContext(request))