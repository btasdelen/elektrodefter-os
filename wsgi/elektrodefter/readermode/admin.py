from django.db import models
from django.contrib import admin
from .models import Category

# Register your models here.

admin.site.register(Category)