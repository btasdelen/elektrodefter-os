from django import forms

class SearchForm(forms.Form):
    search_query = forms.CharField(label='Ara', max_length=100, required=False)