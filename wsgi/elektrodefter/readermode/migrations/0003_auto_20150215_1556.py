# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('readermode', '0002_auto_20150215_1554'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='image',
            field=models.FilePathField(path=b'/home/bilal/workspace/elektrodefter/public/static/images/categories/'),
            preserve_default=True,
        ),
    ]
