# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('readermode', '0003_auto_20150215_1556'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='category',
            name='image',
        ),
        migrations.AddField(
            model_name='category',
            name='imagename',
            field=models.CharField(default='arduino.png', max_length=40),
            preserve_default=False,
        ),
    ]
