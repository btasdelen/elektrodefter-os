from django.db import models
import os

# Create your models here.

class Category(models.Model):
    BASE_DIR = os.path.dirname(os.path.dirname(__file__))

    name = models.CharField(verbose_name="category", max_length = 11)
    title = models.CharField(verbose_name="title", max_length=20)
    image = models.ImageField(verbose_name="image", upload_to="categories", default="categories/no-image.png")
        
    def __str__(self):
        return 'Category: ' + self.title
